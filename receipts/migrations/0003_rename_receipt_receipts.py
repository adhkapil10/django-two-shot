# Generated by Django 4.2 on 2023-04-19 17:19

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("receipts", "0002_rename_account_accounts"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Receipt",
            new_name="Receipts",
        ),
    ]
