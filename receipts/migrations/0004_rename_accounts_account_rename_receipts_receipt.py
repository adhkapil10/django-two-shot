# Generated by Django 4.2 on 2023-04-19 18:15

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("receipts", "0003_rename_receipt_receipts"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Accounts",
            new_name="Account",
        ),
        migrations.RenameModel(
            old_name="Receipts",
            new_name="Receipt",
        ),
    ]
